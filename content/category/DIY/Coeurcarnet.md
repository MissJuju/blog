Title: Carnet personnalisé
Date: 2019-09-14
Category: DIY

##Troisième D.I.Y: Carnet personnalisé

<u>Matériel:</u>

  * Carnet rigide ( noir de préférence )
  J'ai trouvé le mien à Action

 * Des feutres **POSCA** de toutes les couleurs
 * Un gabari en forme de coeur
 * Des étiquettes à double face repositionnables

![matériel]({filename}/images/carnet1.JPG)

<u>1ère étape:</u>

Coller le gabari au centre du carnet à l'aide des étiquettes repositionnables.

 ![matériel]({filename}/images/carnet2.JPG)

On obtient ceci:



 ![matériel]({filename}/images/carnet3.JPG)

<u>2ème étape:</u>


Faire de petits points colorés tout autour du gabari en inss:istant légèrement ( en terme de quantité de points ) sur les contours du coeur.

![matériel]({filename}/images/carnet4.JPG)

![matériel]({filename}/images/carnet5.JPG)

![matériel]({filename}/images/carnet6.JPG)


Il faut faire en sorte d'avoir des points de couleurs quasiment sur tout le carnet.


![matériel]({filename}/images/carnet7.JPG)

![matériel]({filename}/images/carnet8.JPG)


<u>3ème étape:</u>

Décoller délicatement le gabari du carnet.

![matériel]({filename}/images/carnet11.JPG)


<u>Résultat:</u>

![matériel]({filename}/images/carnet12.JPG)
![matériel]({filename}/images/carnet13.JPG)
![matériel]({filename}/images/carnet14.JPG)