Title: Le plastique dingue
Date: 2019-05-08
Category: DIY

![C'est parti]({filename}/images/categoriediy.jpg)


Dans cette rubrique DIY, je vais vous parler de petites choses faciles à fabriquer soi-même (Do It Yourself = fais le toi-même)
Aujourd'hui, je m'attaque plus particulièrement au plastique dingue !!
C'est une feuille (blanche ou de couleur) sur laquelle on dessine **très gros** un objet/un animal.

Je précise **très gros** car une fois cuit (et oui il faut le mettre au four) il rettrécit et devient 7 fois plus petit.
Impressionnant n'est-ce pas !!

Je vous ai donc préparé un petit tutoriel mais cette fois-ci en image pour créer un aimant en plastique dingue !!

#Matériel
<u>
##Plastique dingue</u>

![Acheter du plastique dingue]({filename}/images/imageplastiquedingue1.jpg)

##<u>Aimants</u>

![Acheter des aimants]({filename}/images/aimant.jpg)

##<u>Peinture blanche (facultative)</u>
Elle sera utile si votre plastique dingue est transparent. Il faudra peindre votre aimant avec cette peinture pour qu'il ne se voit pas par transparence. Il faudra également vous munir d'un pinceau.

![Pour peindre l'aimant]({filename}/images/PeintureBlanche.jpg)

##<u>Crayon de papier</u>
Il servira à s'entraîner à dessiner le motif de votre choix. Il faudra donc que vous aillez également une feuille de brouillon.


![Pour s'entraîner sur une feuille de brouillon]({filename}/images/crayondepapier.jpg)

##<u>Feutres de couleurs et un noir</u>
Pour colorier le dessin et repasser les bords en noir.

![Feutres]({filename}/images/Sharpies.jpg) 
![Feutre noir]({filename}/images/feutrenoirok.jpg)

##<u>Ciseaux</u>
![Pour couper le motif]({filename}/images/ciseauxroses.jpg)

##<u> Type colle à bijoux</u>
Pour coller l'aimant au motif une fois cuit.


![Pour coller l'aimant au motif une fois cuit]({filename}/images/colleàbijoux.jpg)

##<u>Four préchauffé à 150°C</u>

![Four]({filename}/images/four.jpg)

##<u>Plaque + Aluminium</u>

![Plaque]({filename}/images/plaquedefour.jpg)
![Rouleau aluminium]({filename}/images/aluminium.jpg)


#<u>C'est parti !!</u>

  * ##**On s'entraîne**

Pour commencer je dessine le motif de mon choix sur une feuille de brouillon pour être **sur** de savoir le faire correctement.

![Sur feuille de brouillon]({filename}/images/pdfeuillebrouillon.jpg)


  * ##**Et pour de vrai**
  
  On reproduit le dessin sur la partie "rugueuse" du plastique dingue. Toujours au crayon de papier.

![Pour de vrai]({filename}/images/plastiquedinguepourdevraie.jpg)

  * ##** Repasse en Noir**

On repasse tous les trais en noir avec un feutre.

![Repasser]({filename}/images/onrepasseennoir.gif)

  * ##**On colorie**

On colorie le dessin aux **feutres** : c'est plus joli...

![Coloriage]({filename}/images/plastiquedindueoncolorie.jpg)

  * ##**Découpage**

On découpe le dessin.

![Découpage]({filename}/images/découpage.gif)

  * ##**Cuisson (à faire en présence d'un adulte pour les enfants)**

Une fois découpée, votre création est prête pour la cuisson.
 
**Étape 1**: Il faut préchauffer votre four à 150°C (thermostat 5).
Placer le plastique dingue sur une plaque recouverte d'une feuille de papier aluminium.
  ![Plaque]({filename}/images/Surlaplque.jpg)
 
**Étape 2**: Il faut surveiller **de très près** le plastique dingue qui va vouloir se coller à lui-même en rétrécissant.
Quand on voit qui se colle, il faut prendre une cuillère en bois et l'applatir.
 ![dans le four]({filename}/images/Danslefour.jpg)

**Étape 3**: On sort le motif du four, puis on l'applati avec un dico.
![fin]({filename}/images/C'estcuit.jpg)

 * ##**Collage **


![collage]({filename}/images/Onprepare.jpg)

On prépare l'aimant et le motif (qui a été d'abord applati)

![collage]({filename}/images/Colle1.jpg)
![collage]({filename}/images/Colle2.jpg)
 On met un point de colle sur le plastique dingue ET l'aimant puis on les rassemble et on les maintient avec une petite pince du même type que sur la photo ci-dessous:
![collage]({filename}/images/Onmaintient.jpg)

* ## **Final**
 Voici le résultat après 30 minutes de séchage:
![résultat]({filename}/images/Final.jpg)

* ## ** Le conseille de la fin ;)**

Si vous hésitez sur la taille de votre dessin sur la feuille de plastique dingue, je vous ai préparé un PDF de manière à ce que vous ayez un gabari.


[- Nuage licorne]({filename}/pdf/nuagelicorne.pdf)

 
 


  







