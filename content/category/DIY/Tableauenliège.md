Title: Tableau en liège personnalisé
Date: 2018-10-07
Category: DIY

##Second D.I.Y: mon tableau en liège perso!

Voici le D.I.Y numéro 2 cette fois si en vidéo.
Vous aurez besoin de:

 * peinture acrylique
 * ruban de masquage
 * dessous de plats en liège ( peuvent être trouvé en grande surfaces)

C'est un D.I.Y simple et pratique qui ne demande pas beaucoup de matériel mais au moins de jour de travail ( à cause du temps de séchage de la peinture ).

 **<u>Le petit +:**</u>
 Malgré le fait que le liège absorbe vite la peinture, il faut prévoir au minimum 1h 30 de séchage par couche de peinture appliquée !!! Si vous n'attendez pas assez longtemps, la peinture risque de ''baver'' au moment où vous allez enlever le ruban de masquage. 

**Bon visionnage !**
{% video https://missjuju.fred-didier.fr/tableauenliege_852x480.mp4 780 480 https://missjuju.fred-didier.fr/tableauliege_852x480_poster.jpg %}

