Title:Le Dessin
Date:2018-04-30
Category:Dessin

![catégorie dessin]({filename}/images/categoriedessin.jpg)

#Matériel

Dans cette rubrique je vais essentiellement parler des dessins **kawaii**!!!

Mais avant de commencer avec un tuto, je vais vous expliquer de quoi vous aurez besoins !!

##Feutres##
Vous aurez besoin de feutres si possibles de couleurs vives...
Personnellement, j'utilise les marqueurs indélébiles `SHARPIES` qui sont géniaux pour ce type de dessin !! Je vous les conseille !

![feutres à pointe fine et très colorés]({filename}/images/Sharpies.jpg)

##Feuilles##
Le mieux dans le cas présent est d'utiliser des feuilles **BLANCHES** et <u>cartonnées</u>...
Avec une feuille souple, je trouve (encore une fois personnellement) que le rendu du dessin est moins bon que sur une feuille légèrement cartonnée... Mais ce n'est que mon avis...

##Protections##
Comme vous utiliserez des feutres, il faudra prendre des précautions...
Il faudra utiliser un vieux calendrier ou des cartons pour **protéger votre table**.

![Protégez votre table !!!]({filename}/images/tache-stylo-meuble-en-bois.jpg)


##<u>1er TUTO</u>

Ce premier tutoriel concerne le nuage licorne comme dans l'article D.I.Y sur le [plastique dingue]({category}D.I.Y)
Bon visionnage !!

{% video https://missjuju.fred-didier.fr/nuagelicorne_852x480.mp4 780 480 https://missjuju.fred-didier.fr/nuagelicorne_852x480_poster.jpg %}

##<u>2ème TUTO</u>

Ce second tutoriel va vous apprendre à réaliser un joli petit panda endormi !!
Bon visionnage !!

{% video https://missjuju.fred-didier.fr/Panda_852x480.mp4 780 480 https://missjuju.fred-didier.fr/Panda_852x480_poster.jpg %}

