Title:Voici mes petites réalisations
Date:2018-09-09
Category:Mes Réalisations

#Alors...


Dans cette rubrique je vais vous monter (juste pour le plaisir) mes petites créations !!
Il y en aura pour tous les goûts: bijoux, D.I.Y, dessins... Bref vous l'aurez compris, je fais de tout !!!

Je vous laisse donc avec la première création !



### <u>#1</u>
 Ici je vous ai mis en scène des petits aimants kawaii :) N'hésitez pas à aller dans la rubrique [D.I.Y]({category}D.I.Y) pour savoir comment les fabriquer ! 


![Aimants]({filename}/images/Imagecreationsaimants.jpg)

###<u> #2</u>
Ma seconde "création" est un *Bullet Journal*. C'est un organisateur et un journal intime. En fait, c'est un carnet **pointillé** ( pour me mieux se repérer ).
Vous pouvez utiliser un kit spécial Bullet Journal avec tout le matérel fournis ou bien acheter un carnet et des stylos feutres noirs.
Il est très important de numéroter les pages et de créer un index pour mieux trouver vos pages. Vous **pouvez** faire une page de codes du type:

&minus; REPORTE AU LENDEMAIN : &minus;>

&minus; REPORTE AUTRE DATE: <&minus;

**BREF**. Le Bullet Journal: je recommande même pour les gens pas particulièrement fort en dessins ou en caligraphie. Mais je ne vais pas vous le cacher; il faut être un minimum **patient**. C'est une activité qui demande de la concentration et de la détermination, il ne s'agit pas d'arrêter au bout de 45 minutes !! 

### <u>#3</u>

Ma troisième création est une boîte à coton personnalisé.
Matériel:

- Mini boîte de Pringles bien lavée
- Whashi tape
- Papier décoratif ( pas trop fin pour que la boîte soi   couverte à 100% 
- Colle

Il vous suffit de décorer votre petite boîte avec le papier décoratif ( de le fixer avec de la colle ) et le whashi tape ! Il ne vous reste qu'à mettre vos cotons dans la boîte.

C'est **simple**,**rapide** et **super pratique** !

![N'hésitez pas à essayer !]({filename}/images/Boitecotons.jpg) 


### <u>#4</u>

Ma quatriéme création est une petite trousse que j'ai réalisé avec l'aide de ma maman et d'une de ses amie ( merchiii ;) ) lors d'une après midi entre copines.
On va pas se le cacher la couture n'est pas ma qualité première... Mais je suis très fière de cette trousse !
J'ai gardé le patron et je vous indiquerai les mesures pour que vous puissiez la faire à votre tour. C'est le moyen de partager un petit moment sympa avec un proche !

![N'hésitez pas à essayer !]({filename}/images/troussedécor.JPG) 
![N'hésitez pas à essayer !]({filename}/images/troussegrue.JPG) 
![N'hésitez pas à essayer !]({filename}/images/trousse.JPG) 


 