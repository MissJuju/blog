Title: Les astuces de Juju
Date: 2018-12-8
Category:Mes astuces




![catégorie]({filename}/images/categorieastuces.jpg)

Dans cette rubrique je vais vous exposer mes petites astuces.
Elles vont surement, à vous aussi, faciliter votre quotidien.


**<u>Astuce #1</u>**

Réaliser une **to do list** vous aidera beaucoup dans la vie de tous les jours.
Cela consiste à construire (vous même si ça vous amuse) la liste des choses que vous avez à faire puis à les cocher **au fur et à mesure** que vous les avez réalisées. Le fait que tout soit posé sur papier vous libèrera l'esprit et réduira votre stress.

Je vous en ai fabriqué une en format PDF que vous pouvez télécharger puis imprimer en cliquant [ici]({filename}/pdf/Mytodolist.pdf)


**<u>Astuce #2</u>**

Pour organiser vos cours (cette astuce fonctionne surtout pour les collégiens et les collégiennes) vous pouvez utiliser **2 pochettes**: une qui reste dans votre sac et qui contient des feuilles simples, des feuilles doubles, les outils de géométrie etc... et la seconde pochette qui sera plutôt un trieur avec autant de compartiment que de matière où vous avez des document non-collés.
Vous pourrez vider votre pochette de sac dans le trieur (enlever les contrôles corrigés, les feuilles volantes etc...) chaque semaine pour alleger votre sac.



**<u>Astuce #3</u>**

Je sais que beaucoup de collégiens et collégiennes oublient leurs cahiers ou leur carnet de correspondance. Pour ne plus vous faire punir par les professeurs, j'ai une technique infaillible. Elle consiste à réaliser votre **<u> propre emploi du temps</u>**. Vous devrez mettre des couleurs et le faire assez gros pour qu'il soit plus liblible que sur votre carnet de correspondance. Vous l'afficherez par exemple sur votre bureau ou sur votre porte d'entrée (faîtes-le en plusieurs exemplaire pour que vos parents en ai un et sache quels sont vos horaires de cours). En lisant cet emploi du temps, vous ferez votre sac **le soir** pour ne pas avoir à le faire le matin et oublier la moitié de vos cours. 
Voici la vidéo que j'ai réalisée pour vous montrer comment faire votre emploi du temps:
 

{% video https://missjuju.fred-didier.fr/Emploidutemps_852x480.mp4 780 480 https://missjuju.fred-didier.fr/Emploidutemps_852x480_poster.jpg %}


**<u>Astuce #4:</u>**    Révisions Examen/Contrôle

**N°1**: <u>Les fiches de révisions</u>  
Les fiches de révisions sont quasi indispensables pour réviser **efficacement**. Elles doivent donner envie d'apprendre même si ce qu'il y a dessus n'est pas forcement passionnant... Pour cela, elles doivent être **colorées** et l'écriture doit être **soignée**. Et même si ça n'est pas facile au début, elles doivent être faites **rapidement**: 15 minutes MAXIMUM !!!!
Elles ne doivent contenir que l'**ESSENTIEL** de la leçon traitée.
Vous pouvez utiliser des symboles comme:   
Un coeur = savoir par coeur   
Une étoile = date importante ( très utilisé en histoire )   
Un triangle = à retenir

**N°2**: <u>Toujours en avance !!</u>  
Situation: Fin du second trimestre, vous êtes en cours d'histoire. Vous n'avez AUCUNE note pour ce trimestre dans cette matière. Vous terminez la leçon avant la sonnerie. Le professeur vous annonce que demain ( le dernier jour avant les vacances ) vous aurez ** UNE ÉVALUATION**!!!!!

Le soir, en rentrant, vous ne perdez pas une minute !!! ( même si vous mourrez d'envie de porter plainte contre le prof pour évaluation une veille de vacances )
Vous filez réviser votre leçon !!
Ça doit maintenant faire 1 heure que vous essayez de l'apprendre; rien à faire, vous n'y arrivez pas, sans parler des définitions !!!!
Découragé(e), vous abandonnez sans même en toucher un mot à vos parents.

Mais peut être quand aillant relue la leçon après chaque cours, vous n'en seriez pas là !!
Bon d'accord je ne le fait peut être pas toujours... Mais que ça nous serve de leçon !

Relire sa leçon régulièrement **même si le prof ne le dit pas** peut être d'une grande aide. Avant et après chaque cours lisez attentivement votre leçon.
De cette manière, il sera beaucoup plus facile de la réviser pour le jour du contrôle. 

**<u>Astuce #5:</u>** Le " BULLET JOURNAL"

Vous savez surement ce dont il s'agit mais au cas où vous ne vous seriez pas connecté sur Internet depuis 2012 je vais vous l'expliquer...

C'est un agenda, un organisateur, un journal intime, des listes de courses, des rendez - vous importants, dans un seul carnet.
Vous pouvez l'acheter déjà préparé.
Ou bien l'acheter vierge et le faire vous même de A à Z.
Personnellement j'ai choisi un bullet journal vierge avec également une couverture craft personnalisable.

En tous cas je trouve  que c'est vraiment une excellente méthode d'organisation.
Vous pourrez d'ailleurs trouver de très bonnes vidéos sur le sujet sur la chaîne YouTube [Les Astuces de Margaux](https://www.youtube.com/user/lesastucesdemargaux).

![Bullet journal]({filename}/images/bulletjournalmesindispensables.jpg)




Voici quelques exemples de pages de MON bullet journal.
Je vous conseille pour chaque mois de faire une "page de garde" avec un thème et/ou une couleur dominante que vous retrouverez dans les pages suivantes de votre mois.
On peut voir par exemple que pour le mois de Mai, la couleur dominante était le vert pastel et qu'on retrouve cette couleur sur une page hebdomadaire.
C'est une astuce simple si vous ne savez pas trop par où commencer.
Il faut également savoir que si vous voulez vraiment faire les choses bien, il faudra acheter un petit peu de matériel.
Mais pas de panique ! Mais vous n'êtes pas obligé de vous ruiner !!
Si vous ne voulez pas ou ne pouvez pas beaucoup investir, vous pouvez trouver des packs de 3 ou 4 stylos noirs parfaits pour écrire dans votre bujo. C'est très largement suffisant pour commencer.
Voici donc quelques uns de mes indispensables papeterie pour le bullet journal. 

![Bullet journal]({filename}/images/matérielmesindispensables.jpg)

  * Stylos noirs de la marque "uni pin" en taille ( de la pointe la plus fine à la plus épaisse ) 0.05, 0.2, 0.4, 0.5, 0.6, 0.7.
  * Une paire de ciseaux fin ( ça peut toujours servir par exemple pour couper du washi tape )
  * Une règle
  * Je n'en ai pas mis sur la photo mais justement le washi tape c'est super pour le bullet journal, surtout quand on ne sait pas trop dessiner, ou quand il faut camoufler une erreur.
  * Un crayon de papier bien taillé
  * Une gomme
  * Un tube de colle

 C'est vraiment les choses que j'utilise le plus souvent mais j'utilise également des feutres pinceaux SAKURA Koi.
 Voici la référence de chacun de ceux que je possède:
 

  * Bleu #39321
  * Rose #39308
  * Jaune #39296
  * Violet #39334
  * Rose pastel #39300
  * Bleu pastel #39323
  * Vert pastel# 39326

Ils sont vraiment super car ils ne traversent pas le papier mais on ne les trouve pas très facilement et ils sont un peu chers.
Personnellement je m'en sers beaucoup et je ne pense pas prendre d'autres couleurs mais si vous n'êtes pas sûr de vous en servir vous pouvez rester sur des surligneurs ( moins chers, trouvables partout et pratiques ).

Si vous ne savez pas dessiner je peux vous conseiller d'utiliser des autocollants, du washi tape, ou de chercher des doodles sur Internet ( ceux sont de petits dessins regroupés par thèmes très faciles à reproduire )
Vous pouvez trouver l'inspiration sur Pinterest, Instagram, dans des magazines.
Mais ne perdez pas de vue que le bullet journal sert à vous organiser.
Donc les décorations ne sont pas obligatoires !
Personnellement, étant au collège, je n'ai pas besoin de faire toutes les semaines des planners ou des planning car j'ai déjà un agenda scolaire, mais les semaines bien chargés, ou durant des périodes stressantes c'est agréable de tout poser sur papier.
Mais si vous êtes déjà dans la vie active, ou même au milieu de vos études de médecine, je vous conseille de faire les planning de votre semaine le dimanche soir bien tranquillement.
Pour finir, le bullet journal est vraiment indispensable si vous avez tendance à être étourdi, à être stressé, ou constamment sur les nerfs. Mais avec un peu de motivation, il peut également régler tout vos problèmes d'organisation.
Alors n'hésitez pas à vous y mettre dès maintenant, et amusez-vous !!!












