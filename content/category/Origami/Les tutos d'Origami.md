Title: Les tutos d'Origami
Date:2018-04-29
Category:Origami
Tags:Origami
# L'Origami, la patience et vous...


Pour pratiquer cet art du pliage du papier il faudra de la patience, mais, sachez que cette patience vous pourrez ** l'acquérir** tout simplement en vous entraînant !
Au début, ce n'est pas facile de rester concentré(e), mais en voulant **vraiment** y arriver vous pourrez le faire. Je peux en témoigner car je suis moi aussi passée par là comme une bonne majorité de la population XD !!
Alors je vous ai préparé de petites vidéos sous formes de tutoriels pour vous expliquer comment réaliser différents origamis.
**Bon visionnage !!**


![C'est parti]({filename}/images/Origami.jpg)


# Matériel


Vous aurez OBLIGATOIREMENT besoin d'un papier carré (sauf exception)...
Mais je crois que je ne vous apprends pas grand chose sur ce coup là...

Vous pourrez aussi avoir besoin de ciseaux, et d'un plioir.


# ET maintenant... C'EST PARTIIIIIIIII

##<u>Le 1er TUTO</u>

Le premier TUTO n'en ai pas vraiment un... Je m'explique: cette vidéo que je vous ai concoctée parle de l'origami en lui même. Elle répondra à deux ou trois questions que vous vous posez certainement: d'où vient l'origami, à quoi sert cet art, etc...

{% video https://missjuju.fred-didier.fr/origami1_852x480.mp4 780 480 https://missjuju.fred-didier.fr/origami1_852x480_poster.jpg %}

##<u>Le 2ème TUTO</u>

Ce second TUTO d'Origami vous expliquera comment réaliser un coeur. C'est idéal pour la saint Valentin !
{% video https://missjuju.fred-didier.fr/origami2_852x480.mp4 780 480 https://missjuju.fred-didier.fr/origami2_852x480_poster.jpg %}

##<u>Le 3ème TUTO</u>

Le troisième TUTO va vous apprendre à réaliser une grue.
Oui ça n'est pas très original mais au moins c'est intéressant...
{% video https://missjuju.fred-didier.fr/grue_852x480.mp4 780 480 https://missjuju.fred-didier.fr/grue_852x480_poster.jpg %}

##<u>Le 4ème TUTO</u>

Voici le quatrième TUTO d'Origami que je vous ai préparé, mais cette fois-ci, c'est une vidéo **spéciale Halloween**: 
Squelette en Origami ( pour décorer votre table )
Bon visionnage !!

{% video https://missjuju.fred-didier.fr/Halloween_852x480.mp4 780 480 https://missjuju.fred-didier.fr/Halloween_852x480_poster.jpg %}


