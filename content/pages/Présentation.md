Title:Présentation
Date: 2018-04-28
Category:1 **Présentation**
Tags:MissJuju


# <u>Bienvenue sur Les Créations de Juju !!</u>


  ![mon logo](../images/thecreativeprocess.jpg)


J'écris en ce moment mon tout premier article sur mon  blog: **Les Créations de Juju**.
Je vous souhaite la bienvenue sur ce site uniquement consacré à la **créativité** de chacun !!

#<u>Le dessin kawaii</u>

Le dessin fait également parti de mes passions et je vous en parlerai dans la rubrique Dessin. N'hésitez pas à y jeter un coup d'oeil en cliquant juste [ici]({category}Dessin) !
J'aime tout ce qui porte sur les licornes (imaginaire, rêves etc...)
Les tutos présents dans cette rubriques serons donc de type **kawaii** !!!

<u>Exemple</u>:
 nuages à corne, licorne, arc-en-ciel...

Mais je ne suis pas qu'une gamine alors je dessine aussi des choses plus basiques mais toujours avec les yeux qui brillent. 

<u>Exemple</u>: fleur, soleil, cannette de soda...


# <u>Les D.I.Y</u>

Des petits trucs de la vie quotidienne, le plus souvent très créatifs. Il se trouve que **J'ADORE** faire des D.I.Y (**D**o **I**t **Y**ourself) , alors pour trouver ma p'tite catégorie cliquez juste [ici]({category}DIY) !

# <u>Les astuces</u>

J'adore me simplifier la vie, être organiser, être dans de bonnes conditions pour travailler...
Si c'est aussi votre cas mais que vous n'arrivez pas à vous organiser correctement n'hésitez pas à aller faire un tour
 dans la rubrique [Mes astuces]({category}Mes astuces).
 Elle comporte plein de conseils utiles pour tout le monde.

#<u>L'origami</u>

J'aime particulièrement l'Origami. Oui, on peut le dire:**J'ADORE** ça !! Vous trouverez donc une rubrique [Origami]({category}Origami).
Les tutoriels sont sous formes de vidéos que je prends plaisir à écrire, tourner, monter et mettre en ligne moi-même !


#<u>Les PDF</u>

De temps en temps, je fais de petite ''affiches'' kawaii sur le site **Canva**.
J'ai donc décidé de vous faire une petite catégorie [PDF]({category}PDF). Il vous est possible d'imprimer gratuitement mes créations.


#<u>Réussites</u> 
La rubrique " Réussites " vous montre toutes les petites choses que je peux réaliser au quotidien. N'hésitez pas à y faire un tour en cliquant [ici]({category}Réussites) ou bien en passant votre souris sur la barre de rubriques en haut de l'écran. 

#<u>LICENCE</u>
J'ai créé ce blog pour partager ma passion, cependant, je ne souhaite pas que son contenu (texte, images, pdf et vidéos) soit réutilisé et modifié sans mon autorisation.
**Merci d'avance** 



#**Quelques choses sur moi**


Je m'appelle **Julie**.

J'ai **13** ans.

Mes passions sont la danse ( moderne Jazz ), l'origami, les loisirs créatifs en général ;) et mon **blog** !!

**Qualité:** Je suis **très** organisée.

**Défaut (je n'en mets qu'un mais la liste est longue):** Je suis têtue.

**Je filme avec:** Samsung galaxy Tab A6 et maintenant avec un Samsung DV150F

**Je monte avec:** Kinemaster Premium

**Je fais les miniatures avec:** Inshot et maintenant Canva


Je suis très contente de ce que je fais sur ce blog car il me permet de partager ce que je sais et ce que j'aime avec vous !!!
Comme je l'ai dit quelques lignes au dessus, j'ai 13 ans donc évidemment, j'écris comme une adolescente de 13 ans...
Je fais de mon mieux pour produire du contenu de qualité. J'endends par là, des phrases qui ont un sens, sans fautes d'orthographes ( le moins possible ), qui expliquent clairement la petite idée qui m'est passée par la tête.
J'organise " toutes ma créativité " dans un grand cahier à carreaux.
J'y écris tous ce que j'aimerai vous partager avec le matériel, si ça marche... ou pas, les différentes étapes pour réaliser ce projet.


J'ai choisi de faire un blog ( on écrit plus sur un blog que sur des vidéos postées sur un site d'hébergement de vidéos ) car l'écriture est, je trouve, un très bon moyen de partager quelque choses. En postant simplement des vidéos sur un site prévu à cet effet, je n'obtiens pas la même satisfaction qu'en créant mon **propre site** de A à Z.


**REMERCIEMENTS**


Je voulais vraiment remercier mes parents qui m'ont énormément soutenu dans ce gros projet que j'ai pu concrétiser !
Surtout mon **papa** qui ma aidé à mettre en place le blog (infrastructure, mise en ligne etc...)

Alors...
#**MERCIIIIIIIIIIIII !!!**#
 
Sur ces bonnes paroles, je vous souhaite une bonne navigation sur **Les Créations de Juju**


 

 

