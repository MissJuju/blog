#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'MissJuju'
SITENAME = 'Les Créations de Juju'
SITEURL = ''
THEME = './theme/notmyidea_fr/'

PATH = 'content'
STATIC_PATHS = ['images', 'pdf', 'videos']
PROFILE_PICTURE = "Juju.png"

PLUGIN_PATHS = ['pelican-plugins']
PLUGINS = ['liquid_tags.video']

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
#LINKS = (('Pelican', 'http://getpelican.com/'),
#         ('Python.org', 'http://python.org/'),
#         ('Jinja2', 'http://jinja.pocoo.org/'),
#         ('You can modify those links in your config file', '#'),)
 
# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#         ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

DELETE_OUTPUT_DIRECTORY = True
OUTPUT_RETENTION = [".git"]
OUTPUT_PATH = 'public/'